from django.http import HttpResponse,JsonResponse
from .models import *
import string
import random, requests
import json
import datetime
import pytz
import calendar
from django.views.decorators.csrf import csrf_exempt

def greeting(request):
    return HttpResponse("안녕하세요, Hello!! Welcome to the J2K Airline. We hope you enjoy your journey with us. :)")

##Only use this when the DB just created
def generateBasicSettings(request):
    Aircraft.objects.create(aircraft_model="Airbus A330-300",tail_number="HL7587",number_of_seats=270)
    Aircraft.objects.create(aircraft_model="Boeing 737-800",tail_number="HL7560",number_of_seats=180)
    Aircraft.objects.create(aircraft_model="Boeing 747-8i",tail_number="HL7633",number_of_seats=600)

    Airport.objects.create(name="New York JFK",country="USA",time_zone="USA EST")
    Airport.objects.create(name="London LHR",country="UK",time_zone="UK GMT")
    Airport.objects.create(name="Paris CDG",country="France",time_zone="FRA CET")

    return HttpResponse("Success")

##Only use this one to re-schedule the flights
def generateFlightSchedule(request):
    jfk=Airport.objects.get(id=1)
    lhr=Airport.objects.get(id=2)
    cdg=Airport.objects.get(id=3)

    smallA=Aircraft.objects.get(id=2)
    middleA=Aircraft.objects.get(id=1)
    largeA=Aircraft.objects.get(id=3)

    lhr_jfk_d=[[8,30,00],[11,20,00],[19,50,00]]
    lhr_jfk_a=[[11,10,00,0],[14,00,00,0],[22,30,00,0]]

    jfk_lhr_d=[[10,10,00],[18,30,00],[23,00,00]]
    jfk_lhr_a=[[22,10,00,0],[6,30,00,1],[11,5,00,1]]

    lhr_cdg_d=[[9,00,00],[11,50,00],[20,35,00]]
    lhr_cdg_a=[[11,20,00,0],[14,10,00,0],[22,50,00,0]]

    cdg_lhr_d=[[10,10,00],[14,30,00],[18,15,00]]
    cdg_lhr_a=[[10,30,00,0],[14,10,00,0],[18,30,00,0]]

    jfk_cdg_d=[[16,20,00],[22,30,00]]
    jfk_cdg_a=[[5,45,00,1],[11,55,00,1]]

    cdg_jfk_d=[[10,20,00],[18,15,00]]
    cdg_jfk_a=[[12,35,00,0],[20,30,00,0]]

    result={}
    lhrjfk=[]
    lhrcdg=[]
    jfklhr=[]
    jfkcdg=[]
    cdglhr=[]
    cdgjfk=[]
    #lhr->jfk
    flight_id=1
    for month in range(1,13):
        for date in range(1,28):
            for i in range(3):
                departureDate=lhr_jfk_d[i]
                dep_h=departureDate[0]
                dep_m=departureDate[1]
                dep_s=departureDate[2]
                arrivalDate=lhr_jfk_a[i]
                arr_h=arrivalDate[0]
                arr_m=arrivalDate[1]
                arr_s=arrivalDate[2]
                addDates=arrivalDate[3]
                randomNumber = random.randint(1000, 9999)
                flight = Flights(id=flight_id)
                flight.flight_number = "JK"+str(randomNumber)
                flight.departure_airport=lhr
                flight.arrival_airport=jfk
                flight.departure_date_time=datetime.datetime(2018,month,date,dep_h,dep_m,dep_s)
                flight.arrival_date_time=datetime.datetime(2018,month,date+addDates,arr_h,arr_m,arr_s)
                flight.duration="7:40"
                flight.used_aircraft=middleA
                flight.seat_price=random.randint(430,499)
                flight.save()
                flight_id+=1
                temp=['2018',month,date,dep_h,dep_m,dep_s]
                lhrjfk.append(temp)

    #jfk->lhr
    for month in range(1,13):
        for date in range(1,28):
            for i in range(3):
                departureDate=jfk_lhr_d[i]
                dep_h=departureDate[0]
                dep_m=departureDate[1]
                dep_s=departureDate[2]
                arrivalDate=jfk_lhr_a[i]
                arr_h=arrivalDate[0]
                arr_m=arrivalDate[1]
                arr_s=arrivalDate[2]
                addDates=arrivalDate[3]
                randomNumber = random.randint(1000, 9999)
                flight = Flights(id=flight_id)
                flight.flight_number = "JK"+str(randomNumber)
                flight.departure_airport=jfk
                flight.arrival_airport=lhr
                flight.departure_date_time=datetime.datetime(2018,month,date,dep_h,dep_m,dep_s)
                flight.arrival_date_time=datetime.datetime(2018,month,date+addDates,arr_h,arr_m,arr_s)
                flight.duration="7:00"
                flight.used_aircraft=middleA
                flight.seat_price=random.randint(430,499)
                flight.save()
                flight_id+=1
                temp=['2018',month,date,dep_h,dep_m,dep_s]
                jfklhr.append(temp)

    #lhr->cdg
    for month in range(1,13):
        for date in range(23,28):
            for i in range(3):
                departureDate=lhr_cdg_d[i]
                dep_h=departureDate[0]
                dep_m=departureDate[1]
                dep_s=departureDate[2]
                arrivalDate=lhr_cdg_a[i]
                arr_h=arrivalDate[0]
                arr_m=arrivalDate[1]
                arr_s=arrivalDate[2]
                addDates=arrivalDate[3]
                randomNumber = random.randint(1000, 9999)
                flight = Flights(id=flight_id)
                flight.flight_number = "JK"+str(randomNumber)
                flight.departure_airport=lhr
                flight.arrival_airport=cdg
                flight.departure_date_time=datetime.datetime(2018,month,date,dep_h,dep_m,dep_s)
                flight.arrival_date_time=datetime.datetime(2018,month,date+addDates,arr_h,arr_m,arr_s)
                flight.duration="1:20"
                flight.used_aircraft=smallA
                flight.seat_price=random.randint(77,99)
                flight.save()
                flight_id+=1
                temp=['2018',month,date,dep_h,dep_m,dep_s]
                lhrcdg.append(temp)

    #cdg->lhr
    for month in range(1,13):
        for date in range(23,28):
            for i in range(3):
                departureDate=cdg_lhr_d[i]
                dep_h=departureDate[0]
                dep_m=departureDate[1]
                dep_s=departureDate[2]
                arrivalDate=cdg_lhr_a[i]
                arr_h=arrivalDate[0]
                arr_m=arrivalDate[1]
                arr_s=arrivalDate[2]
                addDates=arrivalDate[3]
                randomNumber = random.randint(1000, 9999)
                flight = Flights(id=flight_id)
                flight.flight_number = "JK"+str(randomNumber)
                flight.departure_airport=cdg
                flight.arrival_airport=lhr
                flight.departure_date_time=datetime.datetime(2018,month,date,dep_h,dep_m,dep_s)
                flight.arrival_date_time=datetime.datetime(2018,month,date+addDates,arr_h,arr_m,arr_s)
                flight.duration="1:15"
                flight.used_aircraft=smallA
                flight.seat_price=random.randint(77,99)
                flight.save()
                flight_id+=1
                temp=['2018',month,date,dep_h,dep_m,dep_s]
                cdglhr.append(temp)

    #jfk->cdg
    for month in range(1,13):
        for date in range(23,28):
            for i in range(2):
                departureDate=jfk_cdg_d[i]
                dep_h=departureDate[0]
                dep_m=departureDate[1]
                dep_s=departureDate[2]
                arrivalDate=jfk_cdg_a[i]
                arr_h=arrivalDate[0]
                arr_m=arrivalDate[1]
                arr_s=arrivalDate[2]
                addDates=arrivalDate[3]
                randomNumber = random.randint(1000, 9999)
                flight = Flights(id=flight_id)
                flight.flight_number = "JK"+str(randomNumber)
                flight.departure_airport=jfk
                flight.arrival_airport=cdg
                flight.departure_date_time=datetime.datetime(2018,month,date,dep_h,dep_m,dep_s)
                flight.arrival_date_time=datetime.datetime(2018,month,date+addDates,arr_h,arr_m,arr_s)
                flight.duration="7:25"
                flight.used_aircraft=smallA
                flight.seat_price=random.randint(520,559)
                flight.save()
                flight_id+=1
                temp=['2018',month,date,dep_h,dep_m,dep_s]
                jfkcdg.append(temp)

    #cdg->jfk
    for month in range(1,13):
        for date in range(23,28):
            for i in range(2):
                departureDate=cdg_jfk_d[i]
                dep_h=departureDate[0]
                dep_m=departureDate[1]
                dep_s=departureDate[2]
                arrivalDate=cdg_jfk_a[i]
                arr_h=arrivalDate[0]
                arr_m=arrivalDate[1]
                arr_s=arrivalDate[2]
                addDates=arrivalDate[3]
                randomNumber = random.randint(1000, 9999)
                flight = Flights(id=flight_id)
                flight.flight_number = "JK"+str(randomNumber)
                flight.departure_airport=cdg
                flight.arrival_airport=jfk
                flight.departure_date_time=datetime.datetime(2018,month,date,dep_h,dep_m,dep_s)
                flight.arrival_date_time=datetime.datetime(2018,month,date+addDates,arr_h,arr_m,arr_s)
                flight.duration="8:15"
                flight.used_aircraft=smallA
                flight.seat_price=random.randint(520,559)
                flight.save()
                flight_id+=1
                temp=['2018',month,date,dep_h,dep_m,dep_s]
                cdgjfk.append(temp)

    result['lhrjfk']=lhrjfk
    result['jfklhr']=jfklhr
    result['lhrcdg']=lhrcdg
    result['cdglhr']=cdglhr
    result['jfkcdg']=jfkcdg
    result['cdgjfk']=cdgjfk
    return JsonResponse(result, status=200)

@csrf_exempt
def test(request):
    return HttpResponse("Hello world")

def findFlight(request):
    if request.method == 'GET':
        data = request.GET
        departureApt = data['dep_airport']
        arrivalApt = data['dest_airport']
        try:
            departure = Airport.objects.get(name__contains=departureApt)
            arrival = Airport.objects.get(name__contains=arrivalApt)
        except:
            return JsonResponse({'error':'Sorry.... There are no exist airport(s). Please try to do again...'}, status=500)

        departure_datetime = data['dep_date'].split('-')
        yyyy = int(departure_datetime[0])
        mm = int(departure_datetime[1])
        dd = int(departure_datetime[2])
        flexible = data['is_flex']
        if flexible == 'True':
            flexible_date = True
        else:
            flexible_date = False

        flight_lists=[]
        if flexible_date==True:
            dateFlexible = -2
            min_day = dd+dateFlexible
            median_date = dd
            for addDays in range(-2,3,1):
                dd = median_date + addDays
                print(addDays)
                print("Day : ",dd)
                if mm == 1 or mm == 3 or mm == 5 or mm == 7 or mm == 8 or mm == 10 or mm == 12:
                    if dd == 32:
                        if mm == 12:
                            yyyy+=1
                            mm=1
                            dd=0
                        else:
                            mm+=1
                            dd=0
                elif mm == 4 or mm == 6 or mm == 9 or mm == 11:
                    if dd == 31:
                        mm+=1
                        dd=0
                elif mm == 2:
                    if calendar.isleap(yyyy):
                        if dd == 30:
                            mm+=1
                            dd=0
                    else:
                        if dd == 29:
                            mm+=1
                            dd=0

            max_day = dd
            year = yyyy
            month = mm
            departure_date_min = datetime.datetime.combine(datetime.datetime(year,month,min_day), datetime.time.min)
            departure_date_max = datetime.datetime.combine(datetime.datetime(year,month,max_day), datetime.time.max)

            flight_lists.append(Flights.objects.filter(departure_airport=departure,arrival_airport=arrival,departure_date_time__range=(departure_date_min, departure_date_max)).values())
        else:
            year = yyyy
            month = mm
            day = dd
            departure_date_min = datetime.datetime.combine(datetime.datetime(year,month,day), datetime.time.min)
            departure_date_max = datetime.datetime.combine(datetime.datetime(year,month,day), datetime.time.max)
            flight_lists.append(Flights.objects.filter(departure_airport=departure,arrival_airport=arrival,departure_date_time__range=(departure_date_min, departure_date_max)).values())

        result_lists = []

        if flexible_date == False:
            itemList = flight_lists[0]
            for item in itemList:
                result={}
                result['flight_id'] = str(item['id'])
                result['flight_num'] = str(item['flight_number'])
                result['dep_airport'] = list(Airport.objects.filter(id=item['departure_airport_id']).values('name'))[0]['name']
                result['dest_airport'] = list(Airport.objects.filter(id=item['arrival_airport_id']).values('name'))[0]['name']
                result['dep_datetime'] = item['departure_date_time']
                result['arr_datetime'] = item['arrival_date_time']
                result['duration'] = item['duration']
                result['price'] = str(item['seat_price'])
                result_lists.append(result)

        else:
            for itemList in flight_lists:
                for item in itemList:
                    result={}
                    result['flight_id'] = str(item['id'])
                    result['flight_num'] = str(item['flight_number'])
                    result['dep_airport'] = list(Airport.objects.filter(id=item['departure_airport_id']).values('name'))[0]['name']
                    result['dest_airport'] = list(Airport.objects.filter(id=item['arrival_airport_id']).values('name'))[0]['name']
                    result['dep_datetime'] = item['departure_date_time']
                    result['arr_datetime'] = item['arrival_date_time']
                    result['duration'] = item['duration']
                    result['price'] = str(item['seat_price'])
                    result_lists.append(result)

        return_results = {}
        return_results['flights'] = result_lists

        if return_results['flights'] == []:
            return JsonResponse({'error':'Sorry.... There are no exist flights. Please try to search another day...'}, status=500)
        else:
            return JsonResponse(return_results, status=200)
    else:
        return JsonResponse({'error':'Sorry.... There are no exist flights. Please try to search another day...'}, status=500)

@csrf_exempt
def bookingFlight(request):
    if request.method == 'POST':
        data = request.POST
        flight_id = data.get('flight_id')
        result = data.getlist('passengers')
        numberOfPassengers = int(len(result)/4)
        bookingList = []
        passengerInfo = [result[i:i+4] for i in range(0,len(result),4)]
        try:
            flight_info = Flights.objects.get(id=int(flight_id))
        except:
            return JsonResponse({'error':'Sorry.... There is an error about getting flight informations. Please try to do again...'}, status=500)

        flightNumber = flight_info.flight_number
        aircraftID = flight_info.used_aircraft.id
        numOfTotalSeats=Aircraft.objects.get(id=aircraftID).number_of_seats
        bookingNumber = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
        bookingList.append(Bookings.objects.filter(flight_number=flightNumber))
        tot_passengers_in_that_flight=0
        for booking in bookingList:
            tot_passengers_in_that_flight+=len(booking.values('passengers'))

        if tot_passengers_in_that_flight+numberOfPassengers < numOfTotalSeats:
            bookingStatus = "ON_HOLD"
        else:
            return JsonResponse({'error':'Sorry.... There are no exist seats in that flight. Please try to search another flight...'}, status=500)

        total_price = flight_info.seat_price * numberOfPassengers

        passenger = []
        for i in passengerInfo:
            p1 = Passenger()
            for j in range(len(i)):
                if j == 0:
                    p1.firstname=i[j]
                elif j == 1:
                    p1.surname=i[j]
                elif j == 2:
                    p1.email=i[j]
                elif j==3:
                    p1.phone=i[j]
            p1.save()
            passenger.append(p1)

    booking = Bookings(booking_number=bookingNumber,flight_number=flight_info.flight_number,reserved_seats=numberOfPassengers,status=bookingStatus)
    booking.save()
    for i in passenger:
        booking.passengers.add(i)
    booking.save()

    return_results={}
    return_results['booking_num']=bookingNumber
    return_results['booking_status']=bookingStatus
    return_results['tot_price']=total_price

    return JsonResponse(return_results, status=200)

def paymentMethods(request):
    if request.method == 'GET':
        provider_list = Payment.objects.filter().values()
        return_list=[]
        if len(provider_list)>0:
            for index in range(len(provider_list)):
                provider_info={}
                provider_dict = provider_list[index]
                provider_info['pay_provider_id']=provider_dict['id']
                provider_info['pay_provider_name']=provider_dict['provider']
                return_list.append(provider_info)
        else:
            return JsonResponse({'error':'Sorry.... There are no providers. Please try to do again...'}, status=500)

    print(return_list)
    return HttpResponse(json.dumps(return_list))

@csrf_exempt
def payForBooking(request):
    print(request.method)
    if request.method == "POST":
        s = requests.Session()
        data = request.POST
        # getting invoice from provider
        booking_num = data.get('booking_num')
        provider_id = int(data.get('pay_provider_id'))
        customer_booking_info = Bookings.objects.get(booking_number=booking_num)
        num_passenger = customer_booking_info.passengers.count()
        login={}
        login['username']='sc15j2k'
        login['password']='j2kairline'
        headers = {'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'text/plain'}

        provider_url = Payment.objects.get(id=provider_id).url
        account = Payment.objects.get(id=provider_id).account_number
        login_url = provider_url+"api/login/"
        logout_url = provider_url+"api/logout/"
        reply_login = s.post(login_url,data=login,headers=headers)
        if reply_login.status_code == 200:
            invoice_headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            createInvoice_url = provider_url+"api/createinvoice/"
            invoice_data = {}
            invoice_data['account_num'] = account
            invoice_data['client_ref_num'] = '275184'
            invoice_data['amount'] = str(num_passenger)
            raw_invoice = s.post(createInvoice_url,data=json.dumps(invoice_data),headers=invoice_headers)
            reply_create_invoice = json.loads(raw_invoice.text)
            payprovider_ref_num = reply_create_invoice['payprovider_ref_num']
            stamp_code = reply_create_invoice['stamp_code']
            new_invoice = Invoice()
            new_invoice.reference_number = payprovider_ref_num
            new_invoice.booking_info = customer_booking_info
            new_invoice.paid = False
            new_invoice.electronic_stamp = stamp_code
            new_invoice.save()
            return_data = {}
            return_data['pay_provider_id'] = provider_id
            return_data['invoice_id'] = payprovider_ref_num
            return_data['booking_num'] = booking_num
            return_data['url'] = provider_url
            logout = s.post(logout_url,headers=headers)
            return HttpResponse(json.dumps(return_data))
        else:
            return HttpResponse("Sorry.... There is an error about getting flight invoice. Please try to do again...", status=503)

@csrf_exempt
def finalizeBooking(request):
    if request.method == 'POST':
        data = request.POST
        # getting invoice from provider
        booking_num = data.get('booking_num')
        provider_id = data.get('pay_provider_id')
        stamp = data.get('stamp')
        booking_data = Bookings.objects.get(booking_number=booking_num)
        invoice_data = Invoice.objects.get(booking_info=booking_data)
        invoice_stamp = invoice_data.electronic_stamp
        if invoice_stamp == stamp:
            return_data={}
            booking_data.status="CONFIRMED"
            booking_data.save()
            return_data['booking_num']=booking_num
            return_data['booking_status']=booking_data.status
            return JsonResponse(return_data)
        else:
            return HttpResponse("The electronic stamp does not match from your invoice",status=503)
    else:
        return HttpResponse("Error... ...",status=500)

def bookingStatus(request):
    if request.method == 'GET':
        data = request.GET
        get_booking_num = data.get('booking_num')
        booking_results={}
        try:
            booking_data = Bookings.objects.get(booking_number=get_booking_num)
            searched_flight_num = booking_data.flight_number
            flight_data = Flights.objects.get(flight_number=searched_flight_num)
            searched_booking_num = booking_data.booking_number
            booking_status = booking_data.status
            flight_num = flight_data.flight_number
            dep_airport = flight_data.departure_airport.name
            dest_airport = flight_data.arrival_airport.name
            dep_datetime = flight_data.departure_date_time
            arr_datetime = flight_data.arrival_date_time
            duration = flight_data.duration
            booking_results['booking_num'] = searched_booking_num
            booking_results['booking_status'] = booking_status
            booking_results['flight_num'] = flight_num
            booking_results['dep_airport'] = dep_airport
            booking_results['dest_airport'] = dest_airport
            booking_results['dep_datetime'] = dep_datetime
            booking_results['arr_datetime'] = arr_datetime
            booking_results['duration'] = duration
            return JsonResponse(booking_results, status=200)
        except:
            return HttpResponse("We could not find your booking.... Try again",status=503)
    else:
        return HttpResponse("Wrong request...",status=500)

@csrf_exempt
def cancelBooking(request):
    if request.method == 'POST':
        data = request.POST
        print(data)
        get_booking_num = data.get('booking_num')
        updating_results={}
        try:
            booking_data = Bookings.objects.get(booking_number=get_booking_num)
            searched_flight_num = booking_data.flight_number
            flight_data = Flights.objects.get(flight_number=searched_flight_num)
            searched_booking_num = booking_data.booking_number
            try:
                booking_data.status="CANCELLED"
                booking_data.save()
            except:
                return HttpResponse("We could not cancel your booking now.... Try again",status=503)
            booking_status = booking_data.status
            updating_results['booking_num'] = searched_booking_num
            updating_results['booking_status'] = booking_status
            return JsonResponse(updating_results, status=201)
        except:
            return HttpResponse("We could not find your booking.... Try again",status=503)
    else:
        return HttpResponse("Wrong request...",status=500)
