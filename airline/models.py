from django.db import models
from django.utils import timezone

# aircraft DB
class Aircraft(models.Model):
    aircraft_model = models.CharField(max_length=200)
    tail_number = models.CharField(max_length=200)
    number_of_seats = models.IntegerField()
    def __str__(self):
        return self.aircraft_model

# airports DB
class Airport(models.Model):
    name = models.CharField(max_length=1000)
    country = models.CharField(max_length=200)
    time_zone = models.CharField(max_length=200)

    def __str__(self):
        return self.name

# 
class Flights(models.Model):
    flight_number = models.CharField(max_length=10)
    departure_airport = models.ForeignKey(Airport, on_delete=models.CASCADE,related_name='departure')
    arrival_airport = models.ForeignKey(Airport, on_delete=models.CASCADE,related_name='arrival')
    departure_date_time = models.DateTimeField()
    arrival_date_time = models.DateTimeField()
    duration = models.CharField(max_length=100)
    used_aircraft = models.ForeignKey(Aircraft, on_delete=models.CASCADE)
    seat_price = models.IntegerField()

    def __str__(self):
        return self.flight_number

class Passenger(models.Model):
    firstname = models.TextField()
    surname = models.TextField()
    email = models.TextField()
    phone = models.CharField(max_length=11)

    def __str__(self):
        return self.firstname

class Bookings(models.Model):
    booking_number = models.CharField(max_length=20)
    flight_number = models.CharField(max_length=10)
    reserved_seats = models.IntegerField()
    status = models.CharField(max_length=50)
    passengers = models.ManyToManyField(Passenger)
    def __str__(self):
        return self.booking_number

class Payment(models.Model):
    provider = models.CharField(max_length=50)
    url = models.TextField()
    account_number = models.TextField(blank=True)
    def __str__(self):
        return self.provider

class Invoice(models.Model):
    reference_number = models.CharField(max_length=50)
    booking_info = models.ForeignKey(Bookings, on_delete=models.CASCADE)
    paid = models.BooleanField()
    electronic_stamp = models.CharField(max_length=10)

    def __str__(self):
        return self.reference_number
